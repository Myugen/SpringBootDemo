package com.example.services;

import com.example.entities.Customer;
import com.example.repository.CustomerRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import java.util.List;


@EnableJpaRepositories("com.example.repository")
@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public List<Customer> getCustomersByLastName(String lastName) {
        return customerRepository.findByLastName(lastName);
    }

    public List<Customer> getCustomersFirstNameContaining(String firstName) {
        return customerRepository.findByFirstNameContaining(firstName);
    }
}
