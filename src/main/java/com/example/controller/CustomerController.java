package com.example.controller;

import com.example.entities.Customer;
import com.example.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SuppressWarnings("unused")
@RestController
@RequestMapping(value = "/customers")
public class CustomerController {

    private static Class<CustomerController> applicationClass = CustomerController.class;
    private static final Logger log = LoggerFactory.getLogger(applicationClass);

    @Autowired
    private CustomerService customerService;

    public CustomerService getCustomerService() {
        return customerService;
    }
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody List<Customer> allCustomers() {
        log.debug("####### CustomerController.allCustomers");
        return customerService.getAllCustomers();
    }

    @RequestMapping(value = "/firstname", method = RequestMethod.GET)
    public @ResponseBody List<Customer> getCustomersFirstNameContaining(@RequestParam String firstName) {
        log.debug("####### CustomerController.getCustomersFirstNameContaining - Value: " + firstName);
        return customerService.getCustomersFirstNameContaining(firstName);
    }

    @RequestMapping(value = "/lastname", method = RequestMethod.GET)
    public @ResponseBody List<Customer> getCustomersByLastName(@RequestParam String lastName) {
        log.debug("####### CustomerController.getCustomersByLastName - Value: " + lastName);
        return customerService.getCustomersByLastName(lastName);
    }

}
